let { task, series, watch, src, dest } = require('gulp')
let nodemon = require('gulp-nodemon')
let ts = require("gulp-typescript")
let tsProject = ts.createProject("tsconfig.json")
let paths = { pages: ['src/*.html'] }
let clean = require('gulp-clean')

function copyHtml () {
        return src(paths.pages).pipe(dest("dist"))
}

function compilerTypescript () {
        return tsProject.src().pipe(tsProject()).js.pipe(dest("dist"))
}

function defaultTask () {
        copyHtml()
        compilerTypescript()
        watch('src/*.html', series(copyHtml))
        watch('src/*.ts', series(compilerTypescript))
        nodemon({script: './dist/main.js'})
}

task('clean', function () {
        return src('dist', { read: false}).pipe(clean())
})

exports.default = defaultTask


